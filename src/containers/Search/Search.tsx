import React from "react";
import "./Search.scss";
import { getYoutubeLibraryLoaded } from "../../store/reducers/api";
import {
  getSearchNextPageToken,
  getSearchResults,
} from "../../store/reducers/search";
import * as searchActions from "../../store/actions/search";
import { connect } from "react-redux";
import { getSearchParam } from "../../services/url";
import { VideoList, VideoTypes } from "../../components/VideoList/VideoList";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { AppState } from '../../store/reducers';

type TStateProps = {
  searchResults: VideoTypes[] | undefined,
  youtubeApiLoaded: boolean,
  nextPageToken: string | undefined,
}

type TDispatchProps = {
  searchForVideos: typeof searchActions.forVideos.request
}

type TOwnProps = {}

type TProps = RouteComponentProps & TOwnProps & TStateProps & TDispatchProps;

class Search extends React.Component<TProps> {
  render() {
    return (
      <VideoList
        bottomReachedCallback={this.bottomReachedCallback}
        showLoader={true}
        videos={this.props.searchResults}
      />
    );
  }

  getSearchQuery(): string | null {
    return getSearchParam(this.props.location, "search_query");
  }

  componentDidMount() {
    if (!this.getSearchQuery()) {
      // redirect to home component if search query is not there
      this.props.history.push("/");
    }
    this.searchForVideos();
  }

  componentDidUpdate(prevProps: TProps) {
    if (prevProps.youtubeApiLoaded !== this.props.youtubeApiLoaded) {
      this.searchForVideos();
    }
  }

  searchForVideos(): void {
    const searchQuery = this.getSearchQuery();
    if (this.props.youtubeApiLoaded) {
      this.props.searchForVideos(searchQuery);
    }
  }

  bottomReachedCallback = (): void => {
    if (this.props.nextPageToken) {
      this.props.searchForVideos(
        this.getSearchQuery(),
        25,
        this.props.nextPageToken,
      );
    }
  };
}

const mapDispatchToProps: TDispatchProps = {
  searchForVideos: searchActions.forVideos.request
}

function mapStateToProps(state: AppState): TStateProps {
  return {
    youtubeApiLoaded: getYoutubeLibraryLoaded(state),
    searchResults: getSearchResults(state),
    nextPageToken: getSearchNextPageToken(state),
  };
}

export default withRouter(
  connect<TStateProps, TDispatchProps, TOwnProps, AppState>(
    mapStateToProps,
    mapDispatchToProps
  )(Search)
);
