import React from "react";
import { CommentsHeader } from "./CommentsHeader/CommentsHeader";
import { Comment } from "./Comment/Comment";
import { AddComment } from "./AddComment/AddComment";

interface ICommentsProps {
  comments?: IComment[];
  amountComments: number | undefined;
  className: string;
}
export interface IComment {
  id: string;
  etag: string;
  kind: string;
  snippet: {
    canReply: boolean;
    isPublic: boolean;
    topLevelComment: {
      etag: string;
      id: string;
      kind: string;
      snippet: {
        authorChanelId: {
          value: string;
        };
        authorChanelUrl: string;
        authorDisplayName: string;
        authorProfileImageUrl: string;
        canRate: boolean;
        likeCount: number;
        publishedAt: string;
        textDisplay: string;
        textOriginal: string;
        updatedAt: string;
        videoId: string;
        viewerRating: string;
      };
    };
    totalReplyCount: number;
    videoId: string;
  };
}

export class Comments extends React.Component<ICommentsProps> {
  render(): JSX.Element {
    if (!this.props.comments) {
      return <div />;
    }

    const comments: JSX.Element[] = this.props.comments.map(
      (comment: IComment) => {
        return <Comment comment={comment} key={comment.id} />;
      }
    );
    return (
      <div>
        <CommentsHeader amountComments={this.props.amountComments} />
        <AddComment key="add-comment" />
        {comments}
      </div>
    );
  }
}
