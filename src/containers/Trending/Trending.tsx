import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as videoActions from "../../store/actions/video";
import {
  allMostPopularVideosLoaded,
  getMostPopularVideos,
  getMostPopularVideosNextPageToken
} from "../../store/reducers/videos";
import { getYoutubeLibraryLoaded } from "../../store/reducers/api";
import { VideoList } from "../../components/VideoList/VideoList";
import { AppState } from "../../store/reducers/index";
import { VideoTypes } from "../../components/VideoList/VideoList";

type Props = ITrendingStateProps & ITrendingDispatchProps;

class Trending extends React.Component<Props> {
  componentDidMount(): void {
    this.fetchTrendingVideos();
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.youtubeLibraryLoaded !== this.props.youtubeLibraryLoaded) {
      this.fetchTrendingVideos();
    }
  }

  render(): JSX.Element {
    const loaderActive: boolean = this.shouldShowLoader();
    return (
      <VideoList
        bottomReachedCallback={this.fetchMoreVideos}
        showLoader={loaderActive}
        videos={this.props.videos}
      />
    );
  }

  fetchMoreVideos = (): void => {
    const { nextPageToken } = this.props;
    if (this.props.youtubeLibraryLoaded && nextPageToken) {
      this.props.fetchMostPopularVideos(12, true, nextPageToken);
    }
  };

  fetchTrendingVideos(): void {
    if (this.props.youtubeLibraryLoaded) {
      this.props.fetchMostPopularVideos(20, true);
    }
  }

  shouldShowLoader(): boolean {
    return !this.props.allMostPopularVideosLoaded;
  }
}

interface ITrendingStateProps {
  videos: VideoTypes[];
  youtubeLibraryLoaded: boolean;
  allMostPopularVideosLoaded: boolean;
  nextPageToken: string | undefined;
}
interface ITrendingDispatchProps {
  fetchMostPopularVideos: (
    amount: any,
    loadDescription: any, ///solve this any types 
    nextPageToken?: any
  ) => void;
}

function mapStateToProps(state: AppState): ITrendingStateProps {
  return {
    videos: getMostPopularVideos(state),
    youtubeLibraryLoaded: getYoutubeLibraryLoaded(state),
    allMostPopularVideosLoaded: allMostPopularVideosLoaded(state),
    nextPageToken: getMostPopularVideosNextPageToken(state)
  };
}

function mapDispatchToProps(dispatch: any): ITrendingDispatchProps {
  const fetchMostPopularVideos = videoActions.mostPopular.request;
  return bindActionCreators({ fetchMostPopularVideos }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Trending);
