import { VideoGrid } from '../../../components/VideoGrid/VideoGrid';
import React from 'react';
import './HomeContent.scss';
import {
  getMostPopularVideos,
  getVideosByCategory,
} from '../../../store/reducers/videos';
import { connect } from 'react-redux';
import { InfiniteScroll } from '../../../components/InfiniteScroll/InfiniteScroll';
import { VideoTypes } from '../../../components/VideoList/VideoList';

enum Amount { AMOUNT_TRENDING_VIDEOS = 12 };


/**@TODO define type of videosByCategory */
interface stateProps {
  mostPopularVideos?: any;
  videosByCategory?: any
}

interface HomeContentProps {
  bottomReachedCallback: any;
  showLoader: boolean;
}
type Props = HomeContentProps & stateProps

export class HomeContent extends React.Component<Props> {

  render() {
    const trendingVideos = this.getTrendingVideos();
    const categoryGrids = this.getVideoGridsForCategories();

    return (
      <div className='home-content'>
        <div className='responsive-video-grid-container'>
          <InfiniteScroll
            bottomReachedCallback={this.props.bottomReachedCallback}
            showLoader={this.props.showLoader}
          >
            <VideoGrid title='Trending' videos={trendingVideos} />
            {categoryGrids}
          </InfiniteScroll>
        </div>
      </div>
    );
  }

  getTrendingVideos(): VideoTypes[] {
    return this.props.mostPopularVideos.slice(0, Amount.AMOUNT_TRENDING_VIDEOS);
  }

  getVideoGridsForCategories() {

    const categoryTitles = Object.keys(this.props.videosByCategory || {});
    return categoryTitles.map((categoryTitle: string, index: number): JSX.Element => {
      const videos = this.props.videosByCategory[categoryTitle];
      // the last video grid element should not have a divider
      const hideDivider = index === categoryTitles.length - 1;
      return (
        <VideoGrid
          title={categoryTitle}
          videos={videos}
          key={categoryTitle}
          hideDivider={hideDivider}
        />
      );
    });
  }
}
/**@TODO  type state when type state in redux*/
function mapStateToProps(state: any): stateProps {
  return {
    videosByCategory: getVideosByCategory(state),
    mostPopularVideos: getMostPopularVideos(state),
  };
}
export default connect(mapStateToProps, null)(HomeContent);
