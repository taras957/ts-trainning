import React from "react";
import { connect } from "react-redux";
import * as videoActions from "../../store/actions/video";
import "./Home.scss";
import { SideBar } from "../SideBar/SideBar";
import HomeContent from "./HomeContent/HomeContent";
import { bindActionCreators } from "redux";
import { getYoutubeLibraryLoaded } from "../../store/reducers/api";
import {
  getVideoCategoryIds,
  videoCategoriesLoaded,
  videosByCategoryLoaded
} from "../../store/reducers/videos";
import { AppState } from "../../store/reducers/index";

interface HomeStateProps {
  youtubeLibraryLoaded: boolean;
  videoCategories: any;
  videoCategoriesLoaded: any;
  videosByCategoryLoaded: any;
}
interface HomeDispatchProps {
  fetchMostPopularVideosByCategory(category: string[]): {};
  fetchMostPopularVideos(): any;
  fetchVideoCategories(): any;
}

type IHomeProps = HomeStateProps & HomeDispatchProps;

interface HomeState {
  categoryIndex: number;
}

class Home extends React.Component<IHomeProps, HomeState> {
  constructor(props: IHomeProps) {
    super(props);
    this.state = {
      categoryIndex: 0
    };
  }

  render() {
    return (
      <React.Fragment>
        <SideBar />
        <HomeContent
          bottomReachedCallback={this.bottomReachedCallback}
          showLoader={this.shouldShowLoader()}
        />
      </React.Fragment>
    );
  }

  componentDidMount() {
    if (this.props.youtubeLibraryLoaded) {
      this.fetchCategoriesAndMostPopularVideos();
    }
  }

  componentDidUpdate(prevProps: IHomeProps) {
    if (this.props.youtubeLibraryLoaded !== prevProps.youtubeLibraryLoaded) {
      this.fetchCategoriesAndMostPopularVideos();
    } else if (this.props.videoCategories !== prevProps.videoCategories) {
      this.fetchVideosByCategory();
    }
  }

  fetchVideosByCategory() {
    const categoryStartIndex = this.state.categoryIndex;
    const categories = this.props.videoCategories.slice(
      categoryStartIndex,
      categoryStartIndex + 3
    );
    this.props.fetchMostPopularVideosByCategory(categories);
    this.setState(prevState => {
      return {
        categoryIndex: prevState.categoryIndex + 3
      };
    });
  }

  fetchCategoriesAndMostPopularVideos() {
    this.props.fetchMostPopularVideos();
    this.props.fetchVideoCategories();
  }

  bottomReachedCallback = () => {
    if (!this.props.videoCategoriesLoaded) {
      return;
    }
    this.fetchVideosByCategory();
  };

  shouldShowLoader() {
    if (this.props.videoCategoriesLoaded && this.props.videosByCategoryLoaded) {
      return this.state.categoryIndex < this.props.videoCategories.length;
    }
    return false;
  }
}

function mapStateToProps(state: AppState) {
  return {
    youtubeLibraryLoaded: getYoutubeLibraryLoaded(state),
    videoCategories: getVideoCategoryIds(state),
    videoCategoriesLoaded: videoCategoriesLoaded(state),
    videosByCategoryLoaded: videosByCategoryLoaded(state)
  };
}

function mapDispatchToProps(dispatch: HomeDispatchProps) {
  const fetchMostPopularVideos = videoActions.mostPopular.request;
  const fetchVideoCategories = videoActions.categories.request;
  const fetchMostPopularVideosByCategory =
    videoActions.mostPopularByCategory.request;
  return bindActionCreators(
    {
      fetchMostPopularVideos,
      fetchVideoCategories,
      fetchMostPopularVideosByCategory
    },
    dispatch as any
  );
}
/** @TODO*/
export default connect(mapStateToProps, mapDispatchToProps as any)(Home);
