import React from "react";
import { bindActionCreators } from "redux";
import * as watchActions from "../../store/actions/watch";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getYoutubeLibraryLoaded } from "../../store/reducers/api";
import WatchContent from "./WatchContent/Watchcontent";
import { getSearchParam } from "../../services/url";
import { getChannelId } from "../../store/reducers/videos";
import { getCommentNextPageToken } from "../../store/reducers/comments";
import * as commentActions from "../../store/actions/comment";

import { RouteComponentProps } from "react-router-dom";
import { AppState } from "../../store/reducers/index";
import { ActionI } from "../../store/actions";

interface IWatchStateProps {
  youtubeLibraryLoaded: boolean;
  channelId: string | null | undefined;
  nextPageToken: string | null;
}

interface IWatchDispatchToProps {
  fetchWatchDetails: (
    videoId: string | null,
    channelId: string | null | undefined
  ) => ActionI;
  fetchCommentThread: (
    videoId: string | null,
    nextPageToken: string
  ) => ActionI;
}

type Props = IWatchStateProps & IWatchDispatchToProps & RouteComponentProps;

export class Watch extends React.Component<Props> {
  render() {
    const videoId: string | null = this.getVideoId();
    return (
      <WatchContent
        videoId={videoId}
        channelId={this.props.channelId}
        bottomReachedCallback={this.fetchMoreComments}
        nextPageToken={this.props.nextPageToken}
      />
    );
  }

  componentDidMount() {
    if (this.props.youtubeLibraryLoaded) {
      this.fetchWatchContent();
    }
  }

  componentDidUpdate(prevProps: Props) {
    if (this.props.youtubeLibraryLoaded !== prevProps.youtubeLibraryLoaded) {
      this.fetchWatchContent();
    }
  }

  getVideoId(): string | null {
    return getSearchParam(this.props.location, "v");
  }

  fetchWatchContent(): void {
    const videoId = this.getVideoId();
    if (!videoId) {
      this.props.history.push("/");
    }
    this.props.fetchWatchDetails(videoId, this.props.channelId);
  }

  fetchMoreComments = (): void => {
    if (this.props.nextPageToken) {
      this.props.fetchCommentThread(
        this.getVideoId(),
        this.props.nextPageToken
      );
    }
  };
}

function mapStateToProps(state: AppState, props: Props) {
  return {
    youtubeLibraryLoaded: getYoutubeLibraryLoaded(state),
    channelId: getChannelId(state, props.location, "v"),
    nextPageToken: getCommentNextPageToken(state, props.location),
  };
}

function mapDispatchToProps(dispatch: IWatchDispatchToProps) {
  const fetchWatchDetails = watchActions.details.request;
  const fetchCommentThread = commentActions.thread.request;
  return bindActionCreators(
    { fetchWatchDetails, fetchCommentThread },
    dispatch as any
  );
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps as any)(Watch)
);
