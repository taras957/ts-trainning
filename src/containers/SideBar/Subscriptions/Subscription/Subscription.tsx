import React from "react";
import { Icon, Image, Menu } from "semantic-ui-react";
import "./Subscription.scss";

interface ISubscriptionProps {
  amountNewVideos?: number;
  broadcasting?: boolean;
  label: string;
}

export const Subscription: React.FC<ISubscriptionProps> = (
  props: ISubscriptionProps
): JSX.Element => {
  let rightElement: React.ReactElement | null = null;
  const { broadcasting, amountNewVideos } = props;
  if (broadcasting) {
    rightElement = <Icon name="signal" />;
  } else if (amountNewVideos) {
    rightElement = <span className="new-videos-count">{amountNewVideos}</span>;
  }

  return (
    <Menu.Item>
      <div className="subscription">
        <div>
          <Image src="http://via.placeholder.com/28x28" avatar />
          <span>{props.label}</span>
        </div>
        {rightElement}
      </div>
    </Menu.Item>
  );
};
