import React from "react";
import { Menu } from "semantic-ui-react";
import "./SideBarHeader.scss";

interface ISideBarHeaderProps {
  title: string;
}

export const SideBarHeader: React.FC<ISideBarHeaderProps> = (
  props: ISideBarHeaderProps
): JSX.Element => {
  const heading: string = props.title ? props.title.toUpperCase() : "";
  return (
    <Menu.Item>
      <Menu.Header className="side-bar-header">{heading}</Menu.Header>
    </Menu.Item>
  );
};
