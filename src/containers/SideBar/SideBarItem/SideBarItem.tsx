import React from "react";
import { Icon, Menu } from "semantic-ui-react";
import "./SideBarItem.scss";
import { Link, withRouter, RouteComponentProps } from "react-router-dom";

interface PropsType extends RouteComponentProps {
  icon: string;
  label: string;
  path: string;
}

export class SideBarItem extends React.Component<PropsType> {
  render() {
    // React will ignore custom boolean attributes, therefore we pass a string
    // we use this attribute in our SCSS for styling
    const highlight: string | null = this.shouldBeHighlighted()
      ? "highlight-item"
      : null;
    return (
      <Link to={{ pathname: this.props.path }}>
        <Menu.Item className={["sidebar-item", highlight].join(" ")}>
          <div className="sidebar-item-alignment-container">
            <span>
              <Icon size="large" className={this.props.icon} />
            </span>
            <span>{this.props.label}</span>
          </div>
        </Menu.Item>
      </Link>
    );
  }

  shouldBeHighlighted(): boolean {
    const { pathname } = this.props.location;
    if (this.props.path === "/") {
      return pathname === this.props.path;
    }
    return pathname.includes(this.props.path);
  }
}

export default withRouter(SideBarItem);
