import React from 'react';
import './VideoGridHeader.scss';

type TProps = {
  title: string
}

export const VideoGridHeader: React.FC<TProps> = ({ title }) => {
  return (
    <div className='video-grid-header'>
      <span className='title'>{title}</span>
    </div>
  );
}