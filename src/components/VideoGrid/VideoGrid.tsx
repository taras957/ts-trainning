import React from "react";
import "./VideoGrid.scss";
import { VideoGridHeader } from "./VideoGridHeader/VideoGridHeader";
import { Divider } from "semantic-ui-react";
import { VideoPreview } from "../VideoPreview/VideoPreview";
import { VideoTypes } from "../VideoList/VideoList";

type TProps = {
  title: string;
  videos:  VideoTypes[],
  hideDivider?: boolean,
}

export const VideoGrid: React.FC<TProps> = (props) => {
  if (!props.videos || !props.videos.length) {
    return <div />;
  }
  const gridItems = props.videos.map((video) => {
    return (
      <VideoPreview
        video={video}
        key={video.id}
        pathname="/watch"
        expanded={false}
        horizontal={false}
        search={`?v=${video.id}`}
      />
    );
  });

  const divider = props.hideDivider ? null : <Divider />;
  return (
    <React.Fragment>
      <VideoGridHeader title={props.title} />
      <div className="video-grid">{gridItems}</div>
      {divider}
    </React.Fragment>
  );
}
