import React from "react";
import { SideBar } from "../../containers/SideBar/SideBar";
import { InfiniteScroll } from "../InfiniteScroll/InfiniteScroll";
import "./VideoList.scss";
import { VideoPreview } from "../VideoPreview/VideoPreview";

export type VideoTypes = {
  snippet: {
    publishedAt: string;
    description: string;
    channelId?: string;
    thumbnails: {
      medium: {
        url: string;
      };
    };
    title: string;
    channelTitle: string;
  };
  statistics: {
    viewCount: string;
    commentCount?: number;
  };
  contentDetails?: {
    duration: number;
  };
  id: string;
};

interface VideoListProps {
  showLoader: boolean;
  bottomReachedCallback(): void;
  videos: VideoTypes[] | undefined;
}

export class VideoList extends React.Component<VideoListProps> {
  render() {
    const videoPreviews = this.getVideoPreviews();
    console.log(this.props.videos);
    return (
      <React.Fragment>
        <SideBar />
        <div className="video-list">
          <InfiniteScroll
            bottomReachedCallback={this.props.bottomReachedCallback}
            showLoader={this.props.showLoader}
          >
            {videoPreviews}
          </InfiniteScroll>
        </div>
      </React.Fragment>
    );
  }

  getVideoPreviews(): JSX.Element[] | null {
    if (!this.props.videos || !this.props.videos.length) {
      return null;
    }
    const firstVideo = this.props.videos[0];
    if (!firstVideo.snippet.description) {
      return null;
    }

    return this.props.videos.map(video => (
      <VideoPreview
        horizontal={true}
        expanded={true}
        video={video}
        key={video.id}
        pathname={"/watch"}
        search={"?v=" + video.id}
      />
    ));
  }
}
