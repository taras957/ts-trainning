import React from "react";
import "./AppLayout.scss";
import HeaderNav from "../../containers/HeaderNav/HeaderNav";
import ScrollToTop from "../ScrollToTop/ScrollToTop";

interface AppLayout {
  children: React.ReactNode;
}

export function AppLayout(props: AppLayout): JSX.Element {
  return (
    <ScrollToTop>
      <div className="app-layout">
        <HeaderNav />
        {props.children}
      </div>
    </ScrollToTop>
  );
}
