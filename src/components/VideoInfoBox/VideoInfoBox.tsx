import React from "react";
import "./VideoInfoBox.scss";
import { Image, Button, Divider } from "semantic-ui-react";
import Linkify from "react-linkify";
import { getPublishedAtDateString } from "../../services/date/date-format";
import { getShortNumberString } from "../../services/number/number-format";
import { VideoTypes } from "../VideoList/VideoList";
import { IChannel } from "../../store/reducers/channels";

type TProps = {
  video: VideoTypes;
  channel?: IChannel | null;
  className: string;
};

type TState = {
  collapsed: boolean;
};

type TConfig = {
  descriptionTextClass: string;
  buttonTitle: string;
};

export class VideoInfoBox extends React.Component<TProps, TState> {
  constructor(props: TProps) {
    super(props);
    this.state = {
      collapsed: true,
    };
  }

  render() {
    if (!this.props.video || !this.props.channel) {
      return <div />;
    }

    const descriptionParagraphs = this.getDescriptionParagraphs();
    const { descriptionTextClass, buttonTitle } = this.getConfig();
    const publishedAtString = getPublishedAtDateString(
      this.props.video.snippet.publishedAt
    );

    const { channel } = this.props;
    const buttonText = this.getSubscriberButtonText();
    const channelThumbnail = channel.snippet.thumbnails.medium.url;
    const channelTitle = channel.snippet.title;

    return (
      <div>
        <div className="video-info-box">
          <Image className="channel-image" src={channelThumbnail} circular />
          <div className="video-info">
            <div className="channel-name">{channelTitle}</div>
            <div className="video-publication-date">{publishedAtString}</div>
          </div>
          <Button className="subscribe" color="youtube">
            {buttonText}
          </Button>
          <div className="video-description">
            <div className={descriptionTextClass}>{descriptionParagraphs}</div>
            <Button compact onClick={this.onToggleCollapseButtonClick}>
              {buttonTitle}
            </Button>
          </div>
        </div>
        <Divider />
      </div>
    );
  }

  onToggleCollapseButtonClick = (): void => {
    this.setState((prevState) => {
      return {
        collapsed: !prevState.collapsed,
      };
    });
  };

  getDescriptionParagraphs(): JSX.Element[] | null {
    const videoDescription = this.props.video.snippet
      ? this.props.video.snippet.description
      : null;
    if (!videoDescription) {
      return null;
    }
    return videoDescription.split("\n").map((paragraph, index) => (
      <p key={index}>
        <Linkify>{paragraph}</Linkify>
      </p>
    ));
  }

  getSubscriberButtonText(): string {
    const { channel } = this.props;
    const parsedSubscriberCount = Number(channel!.statistics.subscriberCount);
    const subscriberCount: string = getShortNumberString(parsedSubscriberCount);
    return `Subscribe ${subscriberCount}`;
  }

  getConfig(): TConfig {
    let descriptionTextClass = "collapsed";
    let buttonTitle = "Show More";
    if (!this.state.collapsed) {
      descriptionTextClass = "expanded";
      buttonTitle = "Show Less";
    }
    return {
      descriptionTextClass,
      buttonTitle,
    };
  }
}
