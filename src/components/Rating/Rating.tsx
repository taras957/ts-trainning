import React from "react";
import "./Rating.scss";
import { Icon, Progress } from "semantic-ui-react";
import { getShortNumberString } from "../../services/number/number-format";

interface IRating {
  likeCount: number;
  dislikeCount?: number;
}

export const Rating: React.FC<IRating> = (props: IRating): JSX.Element => {
  let rating: React.ReactElement | null = null;
  let likeCount: number | string | null = props.likeCount !== 0 ? props.likeCount : null;
  let dislikeCount: number | string | null = null;

  if (props.likeCount && props.dislikeCount) {

    const amountLikes: number = props.likeCount;
    const amountDislikes: number = props.dislikeCount;
    const percentagePositiveRatings: number =
      100.0 * (amountLikes / (amountLikes + amountDislikes));

    // Now that we have calculated the percentage, we bring the numbers into a better readable format
    likeCount = getShortNumberString(amountLikes);
    dislikeCount = getShortNumberString(amountDislikes);
    rating = <Progress percent={percentagePositiveRatings} size="tiny" />;
  }
  return (
    <div className="rating">
      <div>
        <Icon name="thumbs up outline" />
        <span>{likeCount}</span>
      </div>
      <div>
        <Icon name="thumbs down outline" />
        <span>{dislikeCount}</span>
      </div>
      {rating}
    </div>
  );
};
