import { SUCCESS } from "../actions";
import { WATCH_DETAILS } from "../actions/watch";
import { COMMENT_THREAD_LIST_RESPONSE } from "../api/youtube-api-response-types";
import { createSelector } from "reselect";
import { COMMENT_THREAD } from "../actions/comment";
import { getSearchParam } from "../../services/url";
import { IComment } from "../../containers/Comments/Comments";
import { AppState } from "./index";
import { IResponseChannels } from "./channels";
/*
@needs refactoring of some any types
*/

export interface CommentsState {
  byId: CommentObjectItem;
  byVideo: {
    [key: string]: {
      nextPageToken: string;
      ids: string[];
    };
  };
}

export interface CommentObjectItem {
  [key: string]: IComment;
}

export interface ResponseItem {
  kind: string;
  items: IComment[];
  etag: string;
  nextPageToken: string;
  pageInfo: {
    resultsPerPage: number;
    totalResults: number;
  };
}
export interface IResponseComments {
  result: ResponseItem;
}
export interface CommentItem {
  nextPageToken: string;
  ids: string[];
}
const initialState: CommentsState = {
  byVideo: {},
  byId: {},
};
export default function (state = initialState, action: any): CommentsState {
  switch (action.type) {
    case COMMENT_THREAD[SUCCESS]:
      return reduceCommentThread(action.response, action.videoId, state);
    case WATCH_DETAILS[SUCCESS]:
      return reduceWatchDetails(action.response, action.videoId, state);
    default:
      return state;
  }
}

function reduceWatchDetails(
  responses: IResponseChannels[],
  videoId: string,
  prevState: CommentsState
): CommentsState {
  const commentThreadResponse: any = responses.find(
    (res: any): boolean => res.result.kind === COMMENT_THREAD_LIST_RESPONSE
  );
  return reduceCommentThread(commentThreadResponse.result, videoId, prevState);
}

function reduceCommentThread(
  response: ResponseItem,
  videoId: string,
  prevState: CommentsState
) {
  if (!response) {
    return prevState;
  }
  const newComments: CommentObjectItem = response.items.reduce(
    (acc: CommentObjectItem, item: IComment): CommentObjectItem => {
      acc[item.id] = item;
      return acc;
    },
    {}
  );

  // if we have already fetched some comments for a particular video
  // we just append the ids for the new comments
  const prevCommentIds: string[] = prevState.byVideo[videoId]
    ? prevState.byVideo[videoId].ids
    : [];
  const commentIds: string[] = [...prevCommentIds, ...Object.keys(newComments)];

  const byVideoComment: CommentItem = {
    nextPageToken: response.nextPageToken,
    ids: commentIds,
  };

  return {
    ...prevState,
    byId: {
      ...prevState.byId,
      ...newComments,
    },
    byVideo: {
      ...prevState.byVideo,
      [videoId]: byVideoComment,
    },
  };
}

/*
 * Selectors
 */
const getCommentIdsForVideo = (
  state: AppState,
  videoId: string | null
): string[] => {
  const comment: CommentItem = state.comments.byVideo[videoId!];
  if (comment) {
    return comment.ids;
  }
  return [];
};
export const getCommentsForVideo = createSelector(
  getCommentIdsForVideo,
  (state: AppState): CommentObjectItem => state.comments.byId,
  (commentIds: string[], allComments: CommentObjectItem): IComment[] => {
    return commentIds.map(
      (commentId: string): IComment => allComments[commentId]
    );
  }
);

const getComment = (state: AppState, location: any): CommentItem => {
  const videoId: string | null = getSearchParam(location, "v");
  return state.comments.byVideo[videoId!]; ///solve without!
};
export const getCommentNextPageToken = createSelector(
  getComment,
  (comment: CommentItem): string | null => {
    console.log(comment);
    return comment ? comment.nextPageToken : null;
  }
);
