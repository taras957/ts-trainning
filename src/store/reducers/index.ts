import apiReducer from "./api";
import { combineReducers } from "redux";
import videosReducer from "./videos";
import channelsReducer from "./channels";
import commentsReducer from "./comments";
import searchReducer from "./search";
import { ApiState } from "./api";
import { ChannelsState } from "./channels";
import { CommentsState } from "./comments";
import { SearchState } from "./search";
import { VideoState } from "./videos";

export interface AppState {
  api: ApiState;
  videos: VideoState;
  channels: ChannelsState;
  comments: CommentsState;
  search: SearchState;
}

const rootReducer = combineReducers<AppState>({
  api: apiReducer,
  videos: videosReducer,
  channels: channelsReducer,
  comments: commentsReducer,
  search: searchReducer
});


export default rootReducer;
