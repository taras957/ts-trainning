import {
  VIDEO_DETAILS,
  WATCH_DETAILS,
  ChannelActionTypes,
} from "../actions/watch";
import { SUCCESS } from "../actions";
import { CHANNEL_LIST_RESPONSE } from "../api/youtube-api-response-types";
import { AppState } from "./index";

export interface IChannel {
  id: string;
  snippet: {
    title: string;
    description: string;
    thumbnails: {
      medium: {
        url: string;
        width: number;
        height: number;
      };
    };
  };
  statistics: {
    subscriberCount: string;
  };
}
export interface ChannelObjectItem {
  [key: string]: IChannel;
}
export interface ChannelsState {
  byId: ChannelObjectItem;
}
export interface IResponseChannels extends Response {
  result: {
    kind: string;
    items: IChannel[];
  };
}

const initialState: ChannelsState = {
  byId: {},
};

export default function (
  state = initialState,
  action: ChannelActionTypes
): ChannelsState {
  switch (action.type) {
    case WATCH_DETAILS[SUCCESS]:
      return reduceWatchDetails(action.response, state);
    case VIDEO_DETAILS[SUCCESS]:
      return reduceVideoDetails(action.response, state);
    default:
      return state;
  }
}

function reduceWatchDetails(
  responses: IResponseChannels[],
  prevState: ChannelsState
): ChannelsState {
  const channelResponse: IResponseChannels | undefined = responses.find(
    (response: IResponseChannels): boolean =>
      response.result.kind === CHANNEL_LIST_RESPONSE
  );
  let channels: ChannelObjectItem = {};
  if (channelResponse && channelResponse.result.items) {
    // we know that there will only be one item
    // because we ask for a channel with a specific id
    const channel: IChannel = channelResponse.result.items[0];
    channels[channel.id] = channel;
  }
  return {
    ...prevState,
    byId: {
      ...prevState.byId,
      ...channels,
    },
  };
}

function reduceVideoDetails(
  responses: IResponseChannels[],
  prevState: ChannelsState
) {
  const channelResponse: IResponseChannels | undefined = responses.find(
    (response: IResponseChannels): boolean =>
      response.result.kind === CHANNEL_LIST_RESPONSE
  );
  let channelEntry: ChannelObjectItem = {};
  if (channelResponse && channelResponse.result.items) {
    // we're explicitly asking for a channel with a particular id
    // so the response set must either contain 0 items (if a channel with the specified id does not exist)
    // or at most one item (i.e. the channel we've been asking for)
    const channel: IChannel = channelResponse.result.items[0];
    channelEntry = {
      [channel.id]: channel,
    };
  }

  return {
    ...prevState,
    byId: {
      ...prevState.byId,
      ...channelEntry,
    },
  };
}

/*
 *   Selectors
 * */
export const getChannel = (
  state: AppState,
  channelId: string | null | undefined
) => {
  if (!channelId) return null;
  return state.channels.byId[channelId];
};
