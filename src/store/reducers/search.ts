import { SEARCH_FOR_VIDEOS } from "../actions/search";
import { REQUEST, SUCCESS } from "../actions";
import { SearchActionTypes } from "../actions/search";
import { AppState } from "./index";
import { VideoTypes } from "../../components/VideoList/VideoList";

interface ThumbnailsObject {
  url: string;
  width: number;
  height: number;
}

export interface ISearch {
  kind: string;
  etag: string;
  id: string;
  snippet: {
    publishedAt: string;
    channelId: string;
    title: string;
    description: string;
    thumbnails: {
      default: ThumbnailsObject;
      medium: ThumbnailsObject;
      high: ThumbnailsObject;
    };
    channelTitle: string;
    liveBroadcastContent: string;
  };
}

export interface SearchState {
  totalResults?: number;
  nextPageToken?: string;
  query?: string;
  results?: VideoTypes[];
}
interface ISearchResponse {
  kind: string;
  etag: string;
  nextPageToken: string;
  regionCode: string;
  searchQuery: string;
  pageInfo: {
    totalResults: number;
    resultsPerPage: number;
  };
  items: VideoTypes[];
}

const initialState: SearchState = {};

export default function(
  state = initialState,
  action: SearchActionTypes
): SearchState {
  switch (action.type) {
    case SEARCH_FOR_VIDEOS[SUCCESS]:
      return reduceSearchForVideos(action.response, action.searchQuery, state);
    case SEARCH_FOR_VIDEOS[REQUEST]:
      // delete the previous search because otherwise our component flickers and shows the
      // previous search results before it shows
      return action.nextPageToken ? state : {};
    default:
      return state;
  }
}

function reduceSearchForVideos(
  response: ISearchResponse,
  searchQuery: string,
  prevState: SearchState
): SearchState {
  let searchResults: VideoTypes[] = response.items.map(
    (item: any): VideoTypes => {
      return {
        ...item,
        id: item.id.videoId
      };
    }
  );
  if (prevState.query === searchQuery) {
    const prevResults: VideoTypes[] = prevState.results || [];
    searchResults = prevResults.concat(searchResults);
  }
  return {
    totalResults: response.pageInfo.totalResults,
    nextPageToken: response.nextPageToken,
    query: searchQuery,
    results: searchResults
  };
}

/*
  Selectors
 */
export const getSearchResults = (state: AppState): VideoTypes[] | undefined =>
  state.search.results;
export const getSearchNextPageToken = (state: AppState): string | undefined =>
  state.search.nextPageToken;
