import { YOUTUBE_LIBRARY_LOADED, ApiAction } from "../actions/api";
import { AppState } from "../../store/reducers/index";

export interface ApiState {
  libraryLoaded: boolean;
}

const initialState: ApiState = {
  libraryLoaded: false
};
export default function(state = initialState, action: ApiAction): ApiState {
  switch (action.type) {
    case YOUTUBE_LIBRARY_LOADED:
      return {
        libraryLoaded: true
      };
    default:
      return state;
  }
}
export const getYoutubeLibraryLoaded = (state: AppState) =>
  state.api.libraryLoaded;
