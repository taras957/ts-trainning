import {
  MOST_POPULAR,
  MOST_POPULAR_BY_CATEGORY,
  VIDEO_CATEGORIES,
} from "../actions/video";
import { SUCCESS } from "../actions";
import { createSelector } from "reselect";
import {
  SEARCH_LIST_RESPONSE,
  VIDEO_LIST_RESPONSE,
} from "../api/youtube-api-response-types";
import { VIDEO_DETAILS, WATCH_DETAILS } from "../actions/watch";
import { getSearchParam } from "../../services/url";
import { VideoTypes } from "../../components/VideoList/VideoList";
import { AppState } from "./index";

interface ItemResult {
  totalResults?: number;
  nextPageToken?: string;
  items?: string[];
}

interface IVideoObject {
  [key: string]: VideoTypes;
}
interface ICategoryObject {
  [key: string]: string;
}
interface IByCategoryMap {
  [key: string]: ItemResult;
}
interface IVideosByCategory {
  [key: number]: ItemResult;
}
interface ICategory {
  etag: string;
  id: number;
  kind: string;
  snippet: {
    assignable: boolean;
    channelId: string;
    title: string;
  };
}
type IById = {
  [key: string]: VideoTypes;
};

export interface VideoState {
  byId: IById;
  mostPopular: ItemResult;
  categories: ICategoryObject;
  byCategory: IByCategoryMap;
  related: IByCategoryMap;
}
interface IRelatedResponse {
  result: {
    kind: string;
    items: VideoTypes[];
    etag: string;
    nextPageToken: string;
    regionCode: string;
    pageInfo: {
      totalResults: number;
      resultsPerPage: number;
    };
  };
}
interface IVideoDetails {
  result: {
    kind: string;
    items: VideoTypes[];
  };
}
export interface IVideoResponse {
  nextPageToken: string;
  pageInfo: {
    totalResults: number;
  };
  items: VideoTypes[];
}
export interface IVideoResponseByCategory {
  result: {
    nextPageToken: string;
    pageInfo: {
      totalResults: number;
    };
    items: VideoTypes[];
  };
  status: number;
}
export interface ICategoryResponse {
  etag: string;
  kind: string;
  items: ICategory[];
}
export const initialState: VideoState = {
  byId: {},
  mostPopular: {},
  categories: {},
  byCategory: {},
  related: {},
};
export default function videos(state = initialState, action: any): VideoState {
  switch (action.type) {
    case MOST_POPULAR.SUCCESS:
      return reduceFetchMostPopularVideos(action.response, state);
    case VIDEO_CATEGORIES.SUCCESS:
      return reduceFetchVideoCategories(action.response, state);
    case MOST_POPULAR_BY_CATEGORY.SUCCESS:
      return reduceFetchMostPopularVideosByCategory(
        action.response,
        action.categories,
        state
      );
    case WATCH_DETAILS[SUCCESS]:
      return reduceWatchDetails(action.response, state);
    case VIDEO_DETAILS[SUCCESS]:
      return reduceVideoDetails(action.response, state);
    default:
      return state;
  }
}

function reduceFetchMostPopularVideos(
  response: IVideoResponse,
  prevState: VideoState
): VideoState {
  const videoMap: IVideoObject = response.items.reduce(
    (accumulator: IVideoObject, video: VideoTypes): IVideoObject => {
      accumulator[video.id] = video;
      return accumulator;
    },
    {}
  );

  let items: string[] = Object.keys(videoMap);
  if (response.hasOwnProperty("prevPageToken") && prevState.mostPopular) {
    items = [...prevState.mostPopular.items!, ...items];
  }

  const mostPopular: ItemResult = {
    totalResults: response.pageInfo.totalResults,
    nextPageToken: response.nextPageToken,
    items,
  };

  return {
    ...prevState,
    mostPopular,
    byId: { ...prevState.byId, ...videoMap },
  };
}

function reduceFetchVideoCategories(
  response: ICategoryResponse,
  prevState: VideoState
): VideoState {
  const categoryMapping: ICategoryObject = response.items.reduce(
    (accumulator: ICategoryObject, category: ICategory): ICategoryObject => {
      accumulator[category.id] = category.snippet.title;
      return accumulator;
    },
    {}
  );
  return {
    ...prevState,
    categories: categoryMapping,
  };
}

function reduceFetchMostPopularVideosByCategory(
  responses: IVideoResponseByCategory[],
  categories: string[],
  prevState: VideoState
): VideoState {
  let videoMap = {};
  let byCategoryMap: IByCategoryMap = {};

  responses.forEach(
    (response: IVideoResponseByCategory, index: number): void => {
      // ignore answer if there was an error
      if (response.status === 400) return;

      const categoryId: string = categories[index];
      const { byId, byCategory } = groupVideosByIdAndCategory(response.result);
      videoMap = { ...videoMap, ...byId };
      byCategoryMap[categoryId] = byCategory;
    }
  );

  // compute new state
  return {
    ...prevState,
    byId: { ...prevState.byId, ...videoMap },
    byCategory: { ...prevState.byCategory, ...byCategoryMap },
  };
}

function groupVideosByIdAndCategory(
  response: IVideoResponse
): {
  byId: IById;
  byCategory: ItemResult;
} {
  const videos: VideoTypes[] = response.items;
  const byId: IById = {};
  const byCategory: ItemResult = {
    totalResults: response.pageInfo.totalResults,
    nextPageToken: response.nextPageToken,
    items: [],
  };

  videos.forEach((video: VideoTypes): void => {
    byId[video.id] = video;

    const items: string[] | undefined = byCategory.items;
    if (items && items) {
      items.push(video.id);
    } else {
      byCategory.items = [video.id];
    }
  });

  return { byId, byCategory };
}

function reduceWatchDetails(
  responses: IRelatedResponse[],
  prevState: VideoState
): VideoState {
  const videoDetailResponse: IRelatedResponse | undefined = responses.find(
    (r) => r.result.kind === VIDEO_LIST_RESPONSE
  );
  // we know that items will only have one element
  // because we explicitly asked for a video with a specific id
  const video: VideoTypes = videoDetailResponse!.result.items[0];
  const relatedEntry: ItemResult = reduceRelatedVideosRequest(responses);

  return {
    ...prevState,
    byId: {
      ...prevState.byId,
      [video.id]: video,
    },
    related: {
      ...prevState.related,
      [video.id]: relatedEntry,
    },
  };
}

function reduceRelatedVideosRequest(responses: IRelatedResponse[]): ItemResult {
  const relatedVideosResponse = responses.find(
    (r: IRelatedResponse): boolean => r.result.kind === SEARCH_LIST_RESPONSE
  );
  const { pageInfo, items, nextPageToken } = relatedVideosResponse!.result;
  const relatedVideoIds = items.map((video: any): string => video.id.videoId);

  return {
    totalResults: pageInfo.totalResults,
    nextPageToken,
    items: relatedVideoIds,
  };
}

function reduceVideoDetails(
  responses: IVideoDetails[],
  prevState: VideoState
): VideoState {
  const videoResponses: IVideoDetails[] = responses.filter(
    (response: IVideoDetails): boolean =>
      response.result.kind === VIDEO_LIST_RESPONSE
  );
  const parsedVideos = videoResponses.reduce(
    (
      videoMap: {
        [key: string]: VideoTypes;
      },
      response: IVideoDetails
    ) => {
      // we're explicitly asking for a video with a particular id
      // so the response set must either contain 0 items (if a video with the id does not exist)
      // or at most one item (i.e. the channel we've been asking for)
      const video: VideoTypes | null = response.result.items
        ? response.result.items[0]
        : null;
      if (!video) {
        return videoMap;
      }
      videoMap[video.id] = video;
      return videoMap;
    },
    {}
  );

  return {
    ...prevState,
    byId: { ...prevState.byId, ...parsedVideos },
  };
}

/* function reduceVideoDetails(responses) {
  const videoResponses = responses.filter(response => response.result.kind === VIDEO_LIST_RESPONSE);
  return videoResponses.reduce((accumulator, response) => {
    response.result.items.forEach(video => {
      accumulator[video.id] = video;
    });
    return accumulator;
  }, {});
}

function reduceRelatedVideos(responses, videoIds) {
  const videoResponses = responses.filter(response => response.result.kind === SEARCH_LIST_RESPONSE);
  return videoResponses.reduce((accumulator, response, index) => {
    const relatedIds = response.result.items.map(video => video.id.videoId);
    accumulator[videoIds[index]] = {
      totalResults: response.result.pageInfo.totalResults,
      nextPageToken: response.result.nextPageToken,
      items: relatedIds
    };
    return accumulator;
  }, {});
} */

/*
 *   Selectors
 * */
const getMostPopular = (state: AppState): ItemResult =>
  state.videos.mostPopular;
export const getMostPopularVideos = createSelector(
  (state: AppState): IById => state.videos.byId,
  getMostPopular,
  (videosById: IById, mostPopular: ItemResult): VideoTypes[] => {
    if (!mostPopular || !mostPopular.items) {
      return [];
    }
    return mostPopular.items.map(
      (videoId: string): VideoTypes => videosById[videoId]
    );
  }
);
export const getVideoCategoryIds = createSelector(
  (state: AppState): ICategoryObject => state.videos.categories,
  (categories: ICategoryObject): string[] => {
    return Object.keys(categories || {});
  }
);

export const getVideosByCategory = createSelector(
  (state: AppState): IByCategoryMap => state.videos.byCategory,
  (state: AppState): IById => state.videos.byId,
  (
    state: AppState
  ): {
    [key: string]: string;
  } => state.videos.categories,
  (
    videosByCategory: IByCategoryMap,
    videosById: IById,
    categories: {
      [key: string]: string;
    }
  ) => {
    return Object.keys(videosByCategory || {}).reduce(
      (
        accumulator: {
          [key: string]: VideoTypes[];
        },
        categoryId: string
      ) => {
        const videoIds: string[] | undefined =
          videosByCategory[categoryId].items;
        const categoryTitle = categories[categoryId];
        accumulator[categoryTitle] = videoIds!.map(
          (videoId: string) => videosById[videoId]
        );
        return accumulator;
      },
      {}
    );
  }
);

export const videoCategoriesLoaded = createSelector(
  (state: AppState): ICategoryObject => state.videos.categories,
  (categories: ICategoryObject): boolean => {
    return Object.keys(categories || {}).length !== 0;
  }
);

export const videosByCategoryLoaded = createSelector(
  (state: AppState): IVideosByCategory => state.videos.byCategory,
  (videosByCategory) => {
    return Object.keys(videosByCategory || {}).length;
  }
);

export const getVideoById = (
  state: AppState,
  videoId: string | null
): VideoTypes => {
  return state.videos.byId[videoId!];
};
const getRelatedVideoIds = (
  state: AppState,
  videoId: string | null
): string[] | undefined => {
  const related: ItemResult = state.videos.related[videoId!];
  return related ? related.items : [];
};
export const getRelatedVideos = createSelector(
  getRelatedVideoIds,
  (state: AppState): IById => state.videos.byId,
  (relatedVideoIds: string[] | undefined, videos: IById): VideoTypes[] => {
    if (relatedVideoIds) {
      // filter kicks out null values we might have
      return relatedVideoIds
        .map((videoId: string): VideoTypes => videos[videoId])
        .filter((video: VideoTypes): VideoTypes => video);
    }
    return [];
  }
);

export const getChannelId = (state: AppState, location: any, name: string) => {
  const videoId: string | null = getSearchParam(location, name);
  const video: VideoTypes = state.videos.byId[videoId!];
  if (video) {
    return video.snippet.channelId;
  }
  return null;
};

export const getAmountComments = createSelector(
  getVideoById,
  (video: VideoTypes) => {
    if (video) {
      return video.statistics.commentCount;
    }
    return 0;
  }
);

export const allMostPopularVideosLoaded = createSelector(
  [getMostPopular],
  (mostPopular: ItemResult): boolean => {
    const amountFetchedItems: number = mostPopular.items
      ? mostPopular.items.length
      : 0;
    return amountFetchedItems === mostPopular.totalResults;
  }
);

export const getMostPopularVideosNextPageToken = createSelector(
  [getMostPopular],
  (mostPopular: ItemResult): string | undefined => {
    return mostPopular.nextPageToken;
  }
);
