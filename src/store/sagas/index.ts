import { all, call, put, fork } from "redux-saga/effects";
import {
  watchMostPopularVideos,
  watchMostPopularVideosByCategory,
  watchVideoCategories,
} from "./video";
import { watchWatchDetails } from "./watch";
import { watchCommentThread } from "./comment";
import { watchSearchForVideos } from "./search";
export default function* () {
  yield all([
    fork(watchMostPopularVideos),
    fork(watchVideoCategories),
    fork(watchMostPopularVideosByCategory),
    fork(watchWatchDetails),
    fork(watchCommentThread),
    fork(watchSearchForVideos),
  ]);
}

/*
 * entity must have a success, request and failure method
 * request is a function that returns a promise when called
 * */

type IRequest = () => Promise<any>;

export interface Response {
  result: {
    items: [];
    kind?: string;
    nextPageToken?: string;
    pageInfo?: {};
  };
}

// interface IEntity {
//   request: (
//     searchQuery: string,
//     nextPageToken?: string,
//     amount?: number
//   ) => {
//     type: string;
//   };
//   success: (
//     response: {},
//     searchQuery?: string
//   ) => {
//     type: string;
//   };
//   failure: (
//     response: {},
//     searchQuery?: string
//   ) => {
//     type: string;
//   };
// }

export function* fetchEntity(
  request: IRequest,
  entity: any,
  ...args: string[]
): Generator<{}, void, Response> {
  try {
    const response: Response = yield call(request);
    // we directly return the result object and throw away the headers and the status text here
    // if status and headers are needed, then instead of returning response.result, we have to return just response.
    yield put(entity.success(response.result, ...args));
  } catch (error) {
    yield put(entity.failure(error, ...args));
  }
}

type Args = {
  amount?: number;
  loadDescription?: boolean;
  nextPageToken: string;
  videoCategoryId?: string;
};

export function ignoreErrors(fn: (...args: Args[]) => any, ...args: Args[]) {
  return () => {
    const ignoreErrorCallback = (response: Response): Response => response;
    return fn(...args).then(ignoreErrorCallback, ignoreErrorCallback);
  };
}

export interface Response {
  body: string;
  headers: {};
  result: {
    items: [];
    kind?: string;
    nextPageToken?: string;
    pageInfo?: {};
  };
  status: number;
}

// import { all, call, put, fork } from "redux-saga/effects";
// import {
//   watchMostPopularVideos,
//   watchMostPopularVideosByCategory,
//   watchVideoCategories
// } from "./video";
// import { watchWatchDetails } from "./watch";
// import { watchCommentThread } from "./comment";
// import { watchSearchForVideos } from "./search";
// import { Promise } from "q";
// export default function*() {
//   yield all([
//     fork(watchMostPopularVideos),
//     fork(watchVideoCategories),
//     fork(watchMostPopularVideosByCategory),
//     fork(watchWatchDetails),
//     fork(watchCommentThread),
//     fork(watchSearchForVideos)
//   ]);
// }

// /*
//  * entity must have a success, request and failure method
//  * request is a function that returns a promise when called
//  * */

// type IRequest = () => Promise<any>;

// interface IEntity {
//   request: (
//     searchQuery: string,
//     nextPageToken?: string,
//     amount?: number
//   ) => {
//     type: string;
//   };
//   success: (
//     response: {},
//     searchQuery?: string
//   ) => {
//     type: string;
//   };
//   failure: (
//     response: {},
//     searchQuery?: string
//   ) => {
//     type: string;
//   };
// }

// export function* fetchEntity(
//   request: IRequest,
//   entity: IEntity,
//   ...args: string[]
// ) {
//   try {
//     const response = yield call(request);
//     // we directly return the result object and throw away the headers and the status text here
//     // if status and headers are needed, then instead of returning response.result, we have to return just response.
//     yield put(entity.success(response.result, ...args));
//   } catch (error) {
//     yield put(entity.failure(error, ...args));
//   }
// }

// type Args = {
//   amount?: number;
//   loadDescription?: boolean;
//   nextPageToken: any;
//   videoCategoryId?: any;
// };

// export function ignoreErrors(fn: (...args: Args[]) => any, ...args: Args[]) {
//   return () => {
//     const ignoreErrorCallback = (response: any) => response;
//     return fn(...args).then(ignoreErrorCallback, ignoreErrorCallback);
//   };
// }
