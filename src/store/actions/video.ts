import {
  createAction,
  createRequestTypes,
  REQUEST,
  SUCCESS,
  FAILURE,
  ActionI,
  TypeI,
} from "./index";
import { Response } from "../sagas";

interface CategoriesAction {
  request(): ActionI;
  success(response: Response): ActionI;
  failure(response: Response): ActionI;
}
export const VIDEO_CATEGORIES: TypeI = createRequestTypes("VIDEO_CATEGORIES");
export const categories: CategoriesAction = {
  request: () => createAction(VIDEO_CATEGORIES[REQUEST]),
  success: (response) => createAction(VIDEO_CATEGORIES[SUCCESS], { response }),
  failure: (response) => createAction(VIDEO_CATEGORIES[FAILURE], { response }),
};

interface MostPopularAction {
  request(
    amount: number,
    loadDescription: boolean,
    nextPageToken?: string
  ): ActionI;
  success(response: Response): ActionI;
  failure(response: Response): ActionI;
}
export const MOST_POPULAR: TypeI = createRequestTypes("MOST_POPULAR");
export const mostPopular: MostPopularAction = {
  request: (amount, loadDescription, nextPageToken) =>
    createAction(MOST_POPULAR[REQUEST], {
      amount,
      loadDescription,
      nextPageToken,
    }),
  success: (response) => createAction(MOST_POPULAR[SUCCESS], { response }),
  failure: (response) => createAction(MOST_POPULAR[FAILURE], { response }),
};

interface MostPopularByCategoryAction {
  request(categories: string[]): ActionI;
  success(response: Response, categories: string[]): ActionI;
  failure(response: Response): ActionI;
}
export const MOST_POPULAR_BY_CATEGORY: TypeI = createRequestTypes(
  "MOST_POPULAR_BY_CATEGORY"
);
export const mostPopularByCategory: MostPopularByCategoryAction = {
  request: (categories) =>
    createAction(MOST_POPULAR_BY_CATEGORY[REQUEST], { categories }),
  success: (response, categories) =>
    createAction(MOST_POPULAR_BY_CATEGORY[SUCCESS], { response, categories }),
  failure: (response) =>
    createAction(MOST_POPULAR_BY_CATEGORY[FAILURE], response),
};
