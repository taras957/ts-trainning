import { createAction } from "./index";

export const YOUTUBE_LIBRARY_LOADED = "YOUTUBE_LIBRARY_LOADED";
export const youtubeLibraryLoaded = createAction.bind(
  null,
  YOUTUBE_LIBRARY_LOADED
);

// needs for code compiling

export interface ApiAction {
  type: typeof YOUTUBE_LIBRARY_LOADED;
  libraryLoaded: boolean;
}
