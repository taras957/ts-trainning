import {
  createAction,
  createRequestTypes,
  FAILURE,
  REQUEST,
  SUCCESS,
  ActionI,
  TypeI,
} from "./index";
import { Response } from "../sagas";

interface DetailsAction {
  request(
    videoId: string | null,
    channelId: string | null | undefined
  ): ActionI;
  success(response: Response[], videoId: string): ActionI;
  failure(response: Response[]): ActionI;
}
export const WATCH_DETAILS: TypeI = createRequestTypes("WATCH_DETAILS");
export const details: DetailsAction = {
  request: (videoId, channelId) =>
    createAction(WATCH_DETAILS[REQUEST], { videoId, channelId }),
  success: (response, videoId) =>
    createAction(WATCH_DETAILS[SUCCESS], { response, videoId }),
  failure: (response) => createAction(WATCH_DETAILS[FAILURE], { response }),
};

interface VideoDetailsAction {
  request: () => void;
  success: (response: Response[]) => ActionI;
  failure: (response: Response[]) => ActionI;
}
export const VIDEO_DETAILS: TypeI = createRequestTypes("VIDEO_DETAILS");
export const videoDetails: VideoDetailsAction = {
  request: () => {
    throw Error("not implemented");
  },
  success: (response) => createAction(VIDEO_DETAILS[SUCCESS], { response }),
  failure: (response) => createAction(VIDEO_DETAILS[FAILURE], { response }),
};

// Needs for code compiling

export interface ChannelActionWatchDetails {
  type: string;
  response: [];
}
export interface ChannelActionVideoDetails {
  type: string;
  response: [];
}

export type ChannelActionTypes =
  | ChannelActionWatchDetails
  | ChannelActionVideoDetails;
