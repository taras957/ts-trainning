import { IComment } from "../../containers/Comments/Comments";
import {
  createAction,
  createRequestTypes,
  FAILURE,
  REQUEST,
  SUCCESS,
  ActionI,
  TypeI,
} from "./index";
import { Response } from "../sagas";

export interface ThreadAction {
  request(videoId: string, nextPageToken: string): ActionI;
  success(response: Response, videoId: string): ActionI;
  failure(response: Response): ActionI;
}

export const COMMENT_THREAD: TypeI = createRequestTypes("COMMENT_THREAD");
export const thread: ThreadAction = {
  request: (videoId, nextPageToken) =>
    createAction(COMMENT_THREAD[REQUEST], { videoId, nextPageToken }),
  success: (response, videoId) =>
    createAction(COMMENT_THREAD[SUCCESS], { response, videoId }),
  failure: (response) => createAction(COMMENT_THREAD[FAILURE], { response }),
};

// Needs for code compiling

export interface CommentAction {
  type: string;
  videoId: string;
  response: {
    kind: string;
    items: IComment[];
    etag: string;
    nextPageToken: string;
    pageInfo: {
      resultsPerPage: number;
      totalResults: number;
    };
  };
}

export type CommentActionTypes = CommentAction;
