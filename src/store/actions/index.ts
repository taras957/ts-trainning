export const REQUEST = "REQUEST";
export const SUCCESS = "SUCCESS";
export const FAILURE = "FAILURE";

export function createRequestTypes(base: string): TypeI {
  if (!base) {
    throw new Error("cannot create request type with base = '' or base = null");
  }
  return [REQUEST, SUCCESS, FAILURE].reduce((acc: TypeI, type: string) => {
    acc[type] = `${base}_${type}`;
    return acc;
  }, {});
}

export function createAction(type: string, payload = {}): ActionI {
  return {
    type,
    ...payload,
  };
}

export interface TypeI {
  [key: string]: string;
}

export interface ActionI {
  type: string;
}
