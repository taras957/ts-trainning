
import {
  createAction,
  createRequestTypes,
  FAILURE,
  REQUEST,
  SUCCESS,
  ActionI,
  TypeI,
} from "./index";
import { Response } from "../sagas";
import { VideoTypes } from "../../components/VideoList/VideoList";

interface ForVideosAction {
  request(
    searchQuery: string|null,
    amount?: number,
    nextPageToken?: string
  ): ActionI;
  success(response: Response, searchQuery: string): ActionI;
  failure(response: Response, searchQuery: string): ActionI;
}
export const SEARCH_FOR_VIDEOS: TypeI = createRequestTypes("SEARCH_FOR_VIDEOS");
export const forVideos: ForVideosAction = {
  request: (searchQuery, nextPageToken, amount) =>
    createAction(SEARCH_FOR_VIDEOS[REQUEST], {
      searchQuery,
      nextPageToken,
      amount,
    }),
  success: (response, searchQuery) =>
    createAction(SEARCH_FOR_VIDEOS[SUCCESS], { response, searchQuery }),
  failure: (response, searchQuery) =>
    createAction(SEARCH_FOR_VIDEOS[FAILURE], { response, searchQuery }),
};

// Needs for code compiling

export interface SearchAction {
  type: string;
  response: {
    kind: string;
    etag: string;
    nextPageToken: string;
    regionCode: string;
    searchQuery: string;
    pageInfo: {
      totalResults: number;
      resultsPerPage: number;
    };
    items: VideoTypes[];
  };
  searchQuery: string;
  nextPageToken: string;
}

export type SearchActionTypes = SearchAction;
