import { Location } from "history";

export const getSearchParam = (location: Location<any>, name: string): string | null => {
  if (!location || !location.search) {
    return null;
  }
  const searchParams = new URLSearchParams(location.search);
  return searchParams.get(name);
};