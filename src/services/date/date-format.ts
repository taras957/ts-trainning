const numbers = '\\d+(?:[\\.,]\\d{0,3})?';
const datePattern = `(${numbers}Y)?(${numbers}M)?(${numbers}D)?`;
const timePattern = `T(${numbers}H)?(${numbers}M)?(${numbers}S)?`;
const pattern = new RegExp(`P(?:${datePattern}(?:${timePattern})?)`);

const TimeMap = <const> ['years', 'months','days', 'hours', 'minutes', 'seconds'];
type Times = typeof TimeMap[number];

type TimeType = {
  [K in Times]: number
}

export function parseISO8601TimePattern(durationString: string) {
  // https://github.com/tolu/ISO8601-duration/blob/master/src/index.js
  const found = durationString.match(pattern);
  return found!.slice(1).reduce((prev, next, idx) => {
      prev[TimeMap[idx]] = parseFloat(next) || 0;
      return prev
    }, {} as TimeType);
}

export function getPublishedAtDateString(iso8601DateString: string): string {
  if (!iso8601DateString) {
    return '';
  }
  const date = new Date(Date.parse(iso8601DateString));
  return date.toDateString();
}

export function getVideoDurationString(iso8601DurationString: string | null): string {
  if (!iso8601DurationString || iso8601DurationString === '') {
    return '';
  }

  // new Date(Date.parse(...)) doesn't work here
  // therefore we are using our regex approach
  let {days, hours, minutes, seconds} = parseISO8601TimePattern(iso8601DurationString);

  let secondsString = seconds.toString();
  let minutesString = minutes.toString();
  let accumulatedHours = days * 24 + hours;

  if (seconds < 10) {
    secondsString = seconds.toString().padStart(2, '0');
  }
  if (minutes < 10 && hours !== 0) {
    minutesString = minutesString.toString().padStart(2, '0');
  }
  if (!accumulatedHours) {
    return [minutesString, secondsString].join(':');
  } else {
    return [accumulatedHours, minutesString, secondsString].join(':');
  }
}




